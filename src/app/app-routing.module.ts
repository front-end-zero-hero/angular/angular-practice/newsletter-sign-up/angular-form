import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NewsletterComponent} from "./page/newsletter/newsletter.component";
import {ThanksComponent} from "./page/thanks/thanks.component";

const routes: Routes = [
  {
    path: '', redirectTo: 'newsletter', pathMatch: 'full'
  },
  {
    path: 'newsletter', component: NewsletterComponent
  },
  {
    path: 'thanks', component: ThanksComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
