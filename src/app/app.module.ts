import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ThanksComponent } from './page/thanks/thanks.component';
import {ReactiveFormsModule} from "@angular/forms";
import { NewsletterComponent } from './page/newsletter/newsletter.component';

@NgModule({
  declarations: [
    AppComponent,
    ThanksComponent,
    NewsletterComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
