import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit{
  form!:FormGroup;
  title = 'newsletter-sign-up-with-success-message';

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.buildForm();
  }

  onSubmit() {
    console.log(this.form);
  }

  get onEmailRequired(): boolean {
   return this.form.controls['email']?.hasError("required") && this.form.controls['email']?.touched;
  }

  get onEmailValid(): boolean {
    return this.form.controls['email']?.hasError("email") && this.form.controls['email']?.touched;
  }

  private buildForm() {
    this.form = this.fb.group({
      email: [undefined, [Validators.required, Validators.email]]
    })
  }
}
