import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-thanks',
  templateUrl: './thanks.component.html',
  styleUrls: ['./thanks.component.scss']
})
export class ThanksComponent {
  constructor(private router: Router) {

  }

  async onDismiss() {
    this.router.navigate(['/newsletter']).then()
  }

}
